# Application Tree-FTP
Finard Alexis   
01/02/2021   

## Introduction

Le projet réalisé met en oeuvre la commande shell [tree](https://fr.wikipedia.org/wiki/Tree_(commande)) à la différence que celle-ci affiche l'arborescence d'un répertoire distant accessible via le protocole applicatif [FTP](https://fr.wikipedia.org/wiki/File_Transfer_Protocol).

#### Compilation et exécution

La compilation et l'exécution se font dans le répertoire `Tree-FTP/`.   
La commande `mvn package` permet de compiler le projet.   
La commande `java -jar target/Tree-FTP-1.0-SNAPSHOT.jar` permet d'exécuter le projet.   

L'exécutable peut prendre plusieurs arguments en paramètre. Il est notamment possible de fournir:
- Le serveur ftp à acceder (obligatoire)
- le nom d'utilisateur pour acceder au serveur (optionnel)
- le mot de passe pour acceder au serveur (optionnel)
- la profondeur maximale à laquelle on souhaite acceder (optionnel, vaut 2 si non fournit)

L'usage de la commande est donc le suivant:   
`java -jar target/Tree-FTP-1.0-SNAPSHOT.jar [serveur ftp] [-profondeur] [nom d'utilisateur] [mot de passe]`

Quelques exemples d'utilisations:   

`java -jar target/Tree-FTP-1.0-SNAPSHOT.jar ftp.ubuntu.com`   
`java -jar target/Tree-FTP-1.0-SNAPSHOT.jar ftp.ubuntu.com -0`   
`java -jar target/Tree-FTP-1.0-SNAPSHOT.jar ftp.ubuntu.com -3`   
`java -jar target/Tree-FTP-1.0-SNAPSHOT.jar ftp.free.fr anonymous`   

#### Démonstration video

![Démonstration vidéo](doc/video.gif)

## Architecture

#### Diagramme de classe

Voici le diagramme de classe représentant le projet

![Diagramme de classe](doc/Diagramme_de_classe.png)   

La classe principale est TreeFTP, elle possède la méthode main. La classe Connexion représente la connexion principale avec le serveur (qui sera demandé dans le main) tandis que SubConnexion représente toutes les connexions présentes après le main (pendant l'exploration des répertoires notamment). La classe TreeCommand représente la commande Tree. La méthode closeConnexion est surchargée dans SubConnexion car son utilisation entraine l'envoie de messages au niveau de la connexion principale (qu'il faut lire afin de pouvoir continuer l'exploration).

#### Gestion d'erreur

Concernant la gestion d'erreur, celle-ci est principalement faite dans la méthode main, lors de la connexion avec le serveur ftp.
	   
    try {
        c = new Connexion(args);

    } catch (UnknownHostException e) { // On attrape cette erreur afin d'avertir l'utilisateur d'une potentielle
				           // erreur de frappe
        System.out.println("Un probleme est survenue lors de la connexion au serveur FTP, merci de vérifier les arguments ");
	    TreeFTP.printArgumentsUsage();
	    return;
    }

Lors d'une tentative de connexion, l'erreur `UnknownHostException` peut être levé s'il est impossible de se connecter à un serveur. Cette impossibilité est principalement due à une erreur de frappe, c'est pour cela que l'on récupère l'exception afin de signaler à l'utilisateur qu'il s'est peut-être trompé.

## Code Samples

##### Récupération du maxdepth
L'objectif de ce code est de modifier la variable maxdepth si jamais un `-` est dans le second argument.   
Etant donné que le `-` est dans la chaine, on décide simplement de considérer args[1] comme un entier, puis on ajoute l'opérateur `-` afin d'annuler celui de la chaîne.   
`if (args.length >1 && args[1].charAt(0) == '-') maxdepth = -Integer.parseInt(args[1]);`
##### Algorithme d'exploration
L'objectif de ce code est de faire le parcours en profondeur du repertoire.   
Si le contenu que l'on regarde est un document, alors on affiche son nom, puis on explore. Sinon on affiche juste son nom.

    if (content[0].charAt(0) == 'd') {
       afficheDir(content[content.length-1],depth);
       explore(content[content.length-1],depth+1);
    }
    else {
    	 afficheFic(content[content.length-1],depth);
    }

##### Utilisation des arguments
L'objectif de ce code est de pouvoir identifier et récupérer le nom d'utilisateur et le mot de passe si ceux-ci sont fournies à l'exécution.   

    String user=null;
    String password=null;
    switch (args.length) { // On regarde le nombre d'argument sachant que le premier est forcément le nom du serveur ftp
       case 2:
	   	if (args[1].charAt(0) != '-') user = args[1]; // Si ce second argument ne contient pas de '-', alors il s'agit du nom d'utilisateur
		break;
	   case 3:
	   	if (args[1].charAt(0) != '-') { // Si ce second argument ne contient pas de '-', alors il s'agit du nom d'utilisateur et l'argument suivant est forcément le password
		   user = args[1];
		   password = args[2];
		}
		else { // Ici le second argument est la profondeur (gérée dans le main), l'argument suivant est forcément l'utilisateur
		   user = args[2];
		}
		break;
	   case 4: // Ici le second argument est la profondeur, le troisième le nom d'utilisateur et le dernier est le password
		user = args[2];
		password = args[3];
	  default:
		break;
				
    }
