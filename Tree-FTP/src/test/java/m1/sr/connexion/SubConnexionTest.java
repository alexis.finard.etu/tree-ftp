package m1.sr.connexion;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.UnknownHostException;
import org.junit.Test;

public class SubConnexionTest {
	@Test
	public void getPortShouldReturnRightValue() throws UnknownHostException, IOException {
		String[] s = {"ftp.ubuntu.com"}; // On est obligé de réalisé une connexion
		String[] exemple_of_Port = {"55","17"};		
		assertEquals(	new SubConnexion(s) {
							public int testPort(String[] arr) {
								return super.getPort(arr);
							}
						}.testPort(exemple_of_Port),
						55*256+17);
		exemple_of_Port[0] = "50";
		exemple_of_Port[1] = "0";
		assertEquals(	new SubConnexion(s) {
			public int testPort(String[] arr) {
				return super.getPort(arr);
			}
		}.testPort(exemple_of_Port),
		50*256);
	}
	
	@Test
	public void getAddressShouldReturnRightValue() throws UnknownHostException, IOException {
		String[] s = {"ftp.ubuntu.com"};
		String[] exemple_of_addr = {"175","17","32","0","0"};		
		assertEquals(	new SubConnexion(s) {
							public String testAddress(String[] arr) {
								return super.getAddress(arr);
							}
						}.testAddress(exemple_of_addr),
					"175.17.32");
		exemple_of_addr[3] = "123";
		exemple_of_addr[2] = "456";
		exemple_of_addr[1] = "789";
		exemple_of_addr[0] = "101112";
		assertEquals(	new SubConnexion(s) {
							public String testAddress(String[] arr) {
								return super.getAddress(arr);
							}
						}.testAddress(exemple_of_addr),
						"101112.789.456");
	}

}

