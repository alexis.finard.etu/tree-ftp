package m1.sr.connexion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
/**
 * Classe représentant une sous connexion, plus précisément une connexion pendant l'exploration
 * @author finard
 *
 */
public class SubConnexion extends Connexion {
	private Connexion c;
	/**
	 * 
	 * @param args
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public SubConnexion(String[] args) throws UnknownHostException, IOException {
		super(args);
	}
	
	/**
	 *  Etablit une sous connexion à partir de la connexion principale
	 * @param c la connexion principale
	 * @throws IOException
	 */
	public SubConnexion(Connexion c) throws IOException {
		super(c);
		c.printer.println("PASV");
		String addr = c.reader.readLine();
		if (addr != null) {
			String sub = addr.substring(addr.indexOf('(')+1,addr.length()-2); // On récupère la sous chaine qui commence par '(' et qui finie par le ')' (le dernier charactère)
			String arr[] = sub.split(",");
			s = new Socket(getAddress(arr),getPort(arr));
			reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
			printer = new PrintWriter(s.getOutputStream(),true);
			this.c = c;
		}
	}
	
	@Override
	public void closeConnexion() throws IOException {
		super.closeConnexion();

		c.reader.readLine();
		c.reader.readLine();

	}
	
	protected String getAddress(String[] arr) {		
		List<String> l = new ArrayList<String>();
		for (int i=0;i<arr.length-2;i++) {
			l.add(arr[i]);
		}
		return String.join(".", l);
	}
	
	protected int getPort(String[] arr) {
		return (Integer.parseInt(arr[arr.length-2])*256)+Integer.parseInt(arr[arr.length-1]);
	}




}
