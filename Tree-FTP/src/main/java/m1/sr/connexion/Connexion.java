package m1.sr.connexion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
/**
 * Classe représentant une connexion à un serveur FTP
 * @author finard
 *
 */
public class Connexion {
	protected Socket s;
	protected PrintWriter printer;
	protected BufferedReader reader;
	/**
	 * 
	 * @param args La liste des arguments fournie par le main
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public Connexion(String[] args) throws UnknownHostException, IOException {
		s = new Socket(args[0], 21);
		printer = new PrintWriter(s.getOutputStream(), true);
		reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
		
		String user=null;
		String password=null;
		switch (args.length) { // On regarde le nombre d'argument sachant que le premier est forcément le nom du serveur ftp
		case 2:
			if (args[1].charAt(0) != '-') user = args[1]; // Si ce second argument ne contient pas de '-', alors il s'agit du nom d'utilisateur
			break;
		case 3:
			if (args[1].charAt(0) != '-') { // Si ce second argument ne contient pas de '-', alors il s'agit du nom d'utilisateur et l'argument suivant est forcément le password
				user = args[1];
				password = args[2];
			}
			else { // Ici le second argument est la profondeur (gérée dans le main), l'argument suivant est forcément l'utilisateur
				user = args[2];
			}
			break;
		case 4: // Ici le second argument est la profondeur, le troisième le nom d'utilisateur et le dernier est le password
			user = args[2];
			password = args[3];
		default:
			break;
				
		}
		if (user == null) user = "anonymous";
		if (password == null) password = "";
		reader.readLine();
		printer.println("USER " + user);
		verifyAnswer(reader.readLine());
		printer.println("PASS" + password);
		verifyAnswer(reader.readLine());

	}
	public PrintWriter getPrinter() {
		return printer;
	}
	public BufferedReader getReader() {
		return reader;
	}
	public Socket getS() {
		return s;
	}
	
	public Connexion(Connexion c) {
		
	}
	private void verifyAnswer(String answer) throws UnknownHostException {
		if (answer.charAt(0) == '5') {
			throw new UnknownHostException();
		}
	}
	/**
	 * Ferme le socket, le printer et le reader
	 * @throws IOException
	 */
	public void closeConnexion() throws IOException {
		if (printer != null) printer.close();
		if (reader != null) reader.close();
		if (s != null) s.close();
	}
}
