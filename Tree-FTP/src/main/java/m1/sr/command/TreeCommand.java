package m1.sr.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import m1.sr.connexion.Connexion;
import m1.sr.connexion.SubConnexion;

/**
 * Classe représentant la commande tree
 * @author finard
 *
 */
public class TreeCommand {
	private Connexion c;
	private int maxDepht;

	/**
	 * L'appel au constructeur correpond à la commande tree
	 * @param c la connexion principale
	 * @param maxDepth la profondeur maximale
	 * @throws IOException
	 */
	public TreeCommand(Connexion c,int maxDepth) throws IOException {
		this.c=c;
		this.maxDepht=maxDepth;
		List<String[]> l = readCurrentDir();
		exploreList(l, 0);
	}
	/**
	 * Lis le repertoire courrant
	 * @return une liste des lignes lues
	 * @throws IOException
	 */
	private List<String[]> readCurrentDir() throws IOException {
		Connexion pasv = new SubConnexion(c);
		List<String[]> res = new ArrayList<String[]>();
		if (pasv.getS() != null) {
			c.getPrinter().println("LIST");
			String line = pasv.getReader().readLine();
			while (line!=null) {			
				res.add(line.split("\\s+"));
				line = pasv.getReader().readLine();
			}
			pasv.closeConnexion();
		}
		else {
			res = null;
		}
		return res;
	}
	
	
	private void changeDir(String dir) throws IOException {
		c.getPrinter().println("CWD " + dir);
		c.getReader().readLine();				
	}
	private void changeParentDir() throws IOException {
		c.getPrinter().println("CDUP");
		c.getReader().readLine();				
	}
	/**
	 *  Explore le répertoire donné en paramètre
	 * @param dir le répertoire à explorer
	 * @param depth la profondeur actuelle 
	 * @throws IOException
	 */
	private void explore(String dir, int depth) throws IOException {
		if (depth <= maxDepht) {
			changeDir(dir);
			List<String[]> l = readCurrentDir();
			exploreList(l, depth);
			changeParentDir();
		}
		
	}
	/**
	 * Explore le répertoire courant donné sous la forme d'une liste
	 * @param l le répertoire courant
	 * @param depth la profondeur actuelle
	 * @throws IOException
	 */
	private void exploreList(List<String[]> l , int depth) throws IOException {
		if (l != null) {
			for (String[] content : l) {
				if (content[0].charAt(0) == 'd') {
					afficheDir(content[content.length-1],depth);
					if (content[0].charAt(9) == 'x') explore(content[content.length-1],depth+1);
				}
				else {
					afficheFic(content[content.length-1],depth);
				}
			}
		}
	}
	private void afficheDir(String string, int depth) {
		System.out.println(affiche(depth)  + "\u001B[34m" + string + "\u001B[0m");
	}
	private void afficheFic(String string, int depth) {
		System.out.println(affiche(depth)  + string);
	}

	private String affiche(int depth) {
		String str="";
		for (int i=0; i < depth ; i++) {
			str = str + "    ";
		}
		return str;
	}

}