package m1.sr;

import java.io.IOException;
import java.net.UnknownHostException;

import m1.sr.command.TreeCommand;
import m1.sr.connexion.Connexion;

/**
 * Classe principale du projet
 * @author finard
 * 
 */
public class TreeFTP {

	public static void main(String[] args) throws IOException {
		Connexion c; // Ceci correspond à la connexion avec le serveur FTP
		
		int maxdepth = 1;
		
		if (args.length == 0) {
			TreeFTP.printArgumentsUsage();
			return;
		}
		try {
			c = new Connexion(args);

		} catch (UnknownHostException e) { // On attrape cette erreur afin d'avertir l'utilisateur d'une potentielle
											// erreur de frappe
			System.out.println(
					"Un probleme est survenue lors de la connexion au serveur FTP, merci de vérifier les arguments ");
			TreeFTP.printArgumentsUsage();
			return;
		}
		if (args.length >1 && args[1].charAt(0) == '-') maxdepth = -Integer.parseInt(args[1]);
		new TreeCommand(c,maxdepth);

		c.closeConnexion();
		
	}

	public static void printArgumentsUsage() {
		System.out.println(
				"Usage: java -jar target/Tree-FTP-1.0-SNAPSHOT.jar [adresse_serveur_ftp] [username] [password]\n"
						+ "username et password étant optionnel.");
	}

}
